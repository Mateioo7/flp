#| 1_2020 |#
#| III |#

(defun rem-div-3(l)
	(car (_rem-div-3 l)) ; _rem-div-3 gives the result in a list
)
#| 
_rem-div-3(l) = {
	nil, l number and l % 3 = 0
	[l], l atom
	U i=1->n [_rem-div-3(li)], otherwise where l = [l1..ln]

)
}
|#
(defun _rem-div-3(l)
	(cond
		((listp l) (list (mapcan #'_rem-div-3 l)))
		((and (numberp l) (= (mod l 3) 0)) nil)
		((atom l) (list l))
	)
)

(defun rem(l k level)
	(cond 
		((listp l) (list (mapcan #'(lambda(_l) (_rem-div-3 _l k (+ 1 level))) l)))
		((= level k) nil)
		(t (list l))
	)
)
; (rem-div-3 '(1 (2 A (3 A)) (6))) => ((1 (2 A (A)) NIL))


#| 2_2020 |#
#| III |#

(defun replace-odd(l e)
	(_replace-odd l e -1)
)
#| 
_replace-odd(l e level) = {
	e, level % 2 == 1
	l, l atom
	U i=1->n _replace-odd(li), otherwise where l = [l1..ln]
}
|#
(defun _replace-odd(l e level)
	(cond
		((listp l) (mapcar #'(lambda (_l) (_replace-odd _l e (+ level 1))) l))
		((= (mod level 2) 1) e)
		(t l)
	)
)
; (replace-odd '(a (b (g)) (c (d (e)) (f))) 'h) => (A (H (G)) (H (D (H)) (F)))


#| 3_2020 |#
#| III |#

(defun replace-zero(l k)
	(_replace-zero l k -1)
)
#| 
_replace-zero(l k level) = {
	0, level = k
	l, l is atom
	U i=1->n _replace-zero(li), l is list where l = [l1..ln]
}
|#
(defun _replace-zero(l k level)
	(cond
		((= level k) 0)
		((atom l) l)
		((listp l) (mapcar #'(lambda(_l) (_replace-zero _l k (+ level 1))) l))
	)
)
; (replace-zero '(a (1 (2 b)) (c (d))) '2) => (A (1 (0 0)) (C (0)))


#| 4_2020 |#
#| III |#

(defun replace-even-next(l)
	(_replace-even-next l)
)
#|  
_replace-even-next(l) = {
	l + 1, l is number and l % 2 = 0
	l, l is atom
	U i=1->n _replace-even-next(li), l is list where l = [l1..ln]
}
|#
(defun _replace-even-next(l)
	(cond
		((and (numberp l) (= (mod l 2) 0)) (+ l 1))
		((atom l) l)
		((listp l) (mapcar #'(lambda(_l) (_replace-even-next _l)) l))
	)
)
; (replace-even-next '(1 s 4 (2 f (7)))) => (1 S 5 (3 F (7)))



#| 1_2019 |#
#| 5 |#
; no of sublists where the last atom is nonnumeric

(defun last-atom-lists(l)
	(_last-atom-lists l 0)
)

(defun conditie(l)
	(cond
		((numberp(car(last l))) nil)
		((atom(car(last l))) t)
		((listp(car(last l))) (conditie (car(last l))))
		(t nil)
	)
)

#| 
_last-atom-lists(l level) = {
	0, l is atom
	1 + Ui=1->n _last-atom-lists(li), conditie(l) is true where l = [l1..ln]
	Ui=1->n _last-atom-lists(li), otherwise where l = [l1..ln]
}
|#
(defun _last-atom-lists(l level)
	(cond
		((atom l) 0) ; for main list elements
		((conditie l) (print L) (+ 1 (apply #'+ (mapcar #'_last-atom-lists l))))
		(t (apply #'+ (mapcar #'_last-atom-lists l)))
	)
)
; (last-atom-lists '(A (B 2) (1 C 4) (D 1 (6 F)) ((G 4) 6) F) )



#| 2_2019 |#
#| 5 |#
; no of sublists of given list where the maximal numerical atom on odd levels is even 

(defun linearize-num(l level) ; level = 0 (ALWAYS INITIALLY)
	(cond
		((and (numberp l) (= (mod level 2) 1)) (list l))
		((atom l) nil)
		(t (apply #'append (mapcar #'(lambda(_l) (linearize-num _l (+ 1 level))) l)))
	)
)

(defun mymax(l mx)
	(cond
		((null l) mx)
		((> (car l) mx) (mymax (cdr l) (car l)))
		(t (mymax (cdr l) mx))
	)
)
; '(1 (2 3) 4 ((5) 6)) -> linearize-num gets (1 4 5) -> mymax gets 5 -> 5 % 2 != 0 -> nil
; '(1 (2 3) 4 ((6) 7)) -> linearize-num gets (1 4 6) -> mymax gets 6 -> 6 % 2 == 0 -> T
(defun conditie2(l)
	(= (mod (mymax (linearize-num l '0) '-9999999) 2) 0)
)

(defun even-max(l level)
	(cond
		((atom l) 0)
		((and (> level 0) (conditie2 l)) (print L) (+ 1 (apply #'+ (mapcar #'(lambda(_l) (even-max _l (+ 1 level))) l))))
		(t (apply #'+ (mapcar #'(lambda(_l) (even-max _l (+ 1 level))) l)))
	)
)
; (even-max '(A (B 2) (1 C 4) (1 (6 F)) (((G) 4) 6)) '0)



#| 3_2019 |#
#| 5 |#
; no of sublists of given list having an odd number of nonnumeric atoms on even levels

(defun linearize-let(l level) ; level = 0 (ALWAYS INITIALLY)
	(cond
		((numberp l) nil)
		((and (atom l) (= (mod level 2) 0)) (list l))
		((atom l) nil)
		(t (apply #'append (mapcar #'(lambda(_l) (linearize-let _l (+ 1 level))) l)))
	)
)

(defun mylen(l)
	(cond
		((null l) 0)
		(t (+ 1 (mylen (cdr l))))
	)
)

(defun conditie3(l)
	(= (mod (mylen (linearize-let l '0)) 2) 1)
)

(defun solution(l)
	(even-len-let l '0)
)

(defun even-len-let(l level)
	(cond
		((atom l) 0)
		((and (> level 0) (conditie3 l) (print L) (+ 1 (apply #'+ (mapcar #'(lambda(_l) (even-len-let _l (+ 1 level))) l)))))
		(t (apply #'+ (mapcar #'(lambda(_l) (even-len-let _l (+ 1 level))) l)))
	)
)
; (solution '(A (B 2) (1 C 4) (1 (6 F)) (((G) 4) 6)) )









