
f([], 0).
f([H|T], S):-
	f(T, S1), 
	!,
	H mod 2 =:= 1,
	S is S1 + H.
f([_|T], S):-
	f(T, S).
% f([1,2,3,4,5,6,7],R).
% returns false after first H even number from second branch, which is the last even number (6 from our example).

% change f so that the predicate returns the no. of odd numbers
g([], 0).
g([H|T], S):-
	g(T, S1),
	H mod 2 =:= 1,
	!, % if H is even, we tell Prolog to not go to any other clause
	S is S1 + 1.
g([_|T], S):-
	g(T, S).


f2([], []).
f2([H|T], [H|S]):-
	f2(T, S).
f2([H|T], S):-
	H mod 2 =:= 1,
	f2(T, S).

% 1_2020 II
/*
candidate(L:given List, E:Integer element of L)
(i, o)
candidate(l1..ln) = {
	l1
	candidate(l2..ln)
}
*/
candidate([E|_], E).
candidate([_|T], E):-
	candidate(T,E).
/*
exists(L:input List, E:Integer element)
(i, i)
exists(l1..ln, e) = {
	true, l1 = e
	false, n = 0
	exists(l2..ln, e), otherwise
}
*/
exists([H|_], H).
exists([_|T], E):-
	exists(T, E).
/*
perm_abs(L:input List, Res:List containing a result)
(i, o)
perm_abs(l1..ln) = perm_abs_aux(l1..ln, 1, [E]), where E = candidate(L)
*/
perm_abs(L, Res):-
	candidate(L, E),
	perm_abs_aux(L, Res, 1, [E]).
/*
% perm_abs_aux(L:given List, Res:List containing a result, Len:Integer length of Col, Col:collector List)
% (i,o,i,i)
perm_abs_aux(l1..ln, len, col1..colm) = {
	col1..colm, condition is met
	perm_abs_aux(l1..ln, len+1, E U col1..colm), abs(col1 - E) < 4 and E is not in col1..colm, where E = candidate(l1..ln)
}
*/
perm_abs_aux(L, Col, Len, Col):-
	length(L, Len), % Len - length of list L
	length(Col, LenCol), % LenCol - length of list Col
	Len =:= LenCol,
	!. % cut to prevent searching for impossible solution
perm_abs_aux(L, Res, Len, [H|T]):-
	candidate(L, E),
	abs(H - E) < 4,
	\+ exists([H|T], E),
	NewLen is Len + 1, % NewLen - updated length of Col
	perm_abs_aux(L, Res, NewLen, [E|[H|T]]).
/*
all_perm_abs(L:input List, Res:List containing all results)
(i, o)
all_perm_abs(l1..ln) = U perm_abs(l1..ln)
*/
all_perm_abs(L, Res):-
	findall(PartialRes, perm_abs(L, PartialRes), Res).



% 2_2020
% I.4
faa([], 0).
faa([H|T], S):-
	faa(T, S1),
	S1 is S - H.

% II.

arr_product(L, K, P, Res):-
	candidate(L, E),
	arr_product_aux(L, K, P, Res, 1, [E]).

% product(L:List, Res:Integer)
product([], 1).
product([H|T], Res1):-
	product(T, Res),
	Res1 is H * Res.

condition(L, P):-
	product(L, Prod),
	Prod =:= P.

% arr_product_aux(L:List, K:Integer, P:Integer, Res:List, Len:Integer, Col:List)
% (i,o,i,i) nedeterministic
arr_product_aux(_, K, P, Col, Len, Col):-
	Len =:= K,
	condition(Col, P),
	!.
arr_product_aux(L, K, P, Res, Len, [H|T]):-
	candidate(L, E),
	\+ exists([H|T], E),
	NewLen is Len + 1,
	arr_product_aux(L, K, P, Res, NewLen, [E|[H|T]]).

all_arr_product(L, K, P, Res):-
	findall(PartialRes, arr_product(L, K, P, PartialRes), Res).


% 3_2020
% I.2
f3([], 0).
f3([H|T], S):-
	f3(T, S1),
	H < S1,
	!,
	S is H + S1.
f3([_|T], S):-
	f3(T, S1),
	S is S1 + 2.

g3([], 0).
g3([H|T], S):-
	g3(T, S1),
	aux(H, S, S1).

aux(H, S, S1):-
	H < S1,
	!,
	S is H + S1.
aux(_, S, S1):-
	S is S1 + 2.

% I.4
p(1).
p(2).

q(1).
q(2).

r(1).
r(2).

s():-
	!,
	p(X),
	q(Y),
	r(Z),
	write(X), write(Y), write(Z),
	nl.

% II
condition(Len, N, Sum):-
	Len >= N,
	Sum mod 3 =:= 0.

sub_sum(L, N, Res):-
	candidate(L, E),
	sub_sum_aux(L, N, Res, 1, E, [E]).

sub_sum_aux(_, N, Col, Len, Sum, Col):-
	condition(Len, N, Sum),
	!.
sub_sum_aux(L, N, Res, Len, Sum, [H|T]):-
	candidate(L, E),
	\+ exists([H|T], E), % to prevent duplicate values
	E < H, % to prevent duplicate sets
	NewSum is Sum + E,
	NewLen is Len + 1,
	sub_sum_aux(L, N, Res, NewLen, NewSum, [E|[H|T]]).

all_sub_sum(L, N, Res):-
	findall(PartialRes, sub_sum(L, N, PartialRes), Res).


% 1_2019
% 2
% conc(L1:List, L2:List, Res:List)
conc(L1, [], L1).
conc([], L2, L2).
conc([H|T], L2, [H|[H1|T1]]):-
	conc(T, L2, [H1|T1]).

% rev(L:List, Res:List)
% (i, o) deterministic
/*
rev(l1..ln, R) = {
	[], n = 0
	l1, l1 is atom
	rev(l1) U rev(l2..ln), l1 is list
}
*/
rev([], []):-!.
rev([H|T], Res):-
	!,
	rev(H, T1),
	rev(T, T2),
	conc(T2, [T1], Res).
rev(E, E).

