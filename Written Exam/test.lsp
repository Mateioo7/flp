(defun remo(l k level)
	(cond 
		((listp l) (list (mapcan #'(lambda(_l) (remo _l k (+ 1 level))) l)))
		((= level k) nil)
		(t (list l))
	)
)