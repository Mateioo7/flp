; 11. Write a function to determine the depth of a list.

; get-list-depth(l1..ln) = _get-list-depth(l1..ln, 0)
(defun get-list-depth(l)
	(_get-list-depth l 0)
)

#|
_get-list-depth(l, depth) = {
	depth, l is atom
	max(_get-list-depth(l1).._get-list-depth(ln)), l = [l1..ln]
}
|#

(defun _get-list-depth(l depth)
	(cond
		((atom l) depth) ; atom('nil) = T => _get-list-depth((), 0) = depth = 0
		((listp l)
			(apply #'max (mapcar #'(lambda(_l) (_get-list-depth _l (+ depth 1))) l) ))
		; mapcar output is the list of each element's depth having same structure as the initial list => the entire list's depth is the max of mapcar
	)
)

; (get-list-depth '(A (B (C) (D E (F)))))           -> 4
; (get-list-depth '(A -2 (3)))						-> 2
; (get-list-depth '(A 2 3))							-> 1
; (get-list-depth '())								-> 0
; (get-list-depth '(1 (b (4) c) (d (3))))			-> 3