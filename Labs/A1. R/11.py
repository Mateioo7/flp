class Node:
    def __init__(self, value):
        self.value = value
        self.next = None


class List:
    def __init__(self):
        self.first = None


def createList():
    list = List()
    list.first = createList_rec()
    return list


def createList_rec():
    x = int(input("x="))
    if x == 0:
        return None
    else:
        node = Node(x)
        node.next = createList_rec()
        return node


def display(list):
    display_rec(list.first)


def display_rec(node):
    if node is not None:
        print(node.value)
        display_rec(node.next)


def elementExists(list, e):
    return elementExists_rec(list.first, e)


def elementExists_rec(node, e):
    if node is None:
        return False
    elif node.value == e:
        return True
    else:
        return elementExists_rec(node.next, e)


def listLength(list):
    return listLength_rec(list.first)


def listLength_rec(node):
    if node is None:
        return 0
    else:
        return 1 + listLength_rec(node.next)


def main():
    list = createList()
    e = int(input("e="))
    print("element " + str(e) + " exists: " + str(elementExists(list, e)))
    print("list length: " + str(listLength(list)))


main()
