/*
5.b. Write a predicate to determine the set of all the pairs of
elements in a list.
Eg.: L = [a b c d] => [[a b] [a c] [a d] [b c] [b d] [c d]].

*/

% pereche(E:element, L:list, R:list)
% (i,i,o) non-deterministic
% pereche(E,l1l2l3..,R) =
% 1. (E,l1)
% 2. pereche(E,l2l3..,R)
pereche(X,[Y|_],[X,Y]).
pereche(X,[_|T],R):-
    pereche(X,T,R).

% perechi(L:list, R:list)
% (i,o) non-deterministic
% perechi(l1l2..,R) =
% 1. pereche(l1,l2l3..,R)
% 2. perechi(l2l3..,R)
perechi([H|T],R):-
    pereche(H,T,R).
perechi([_|T],R):-
    perechi(T,R).

% all_pairs(L:list, R:list)
% (i,o) deterministic
% appends each solution (X) of perechi to R
all_pairs(L,R):-
    findall(X,perechi(L,X),R).

main :- pereche(5,[1,2,3,4], Solution), writef('%t\n', [Solution]).