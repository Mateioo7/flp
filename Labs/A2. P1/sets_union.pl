% 5a. Write a predicate to compute the union of two sets.

% exists(L:list, E:element)
% (i,i) deterministic
% exists(l1l2..,E) =
% 1. true, l1 = E
% 2. exists(l2l3..,E), otherwise
exists([H|_],H):- !.
exists([_|T],E):-
    exists(T,E).

% sets_union(A:list, B:list, R:list)
% (i,i,o) deterministic
% sets_union(a1a2..,B,R) =
% 1. B, A is empty
% 2. sets_union(a2a3..,B,R), if exists(B,a1) is true
% 3. a1 + sets_union(a2a3..,B,R), if exists(B,a1) is false
sets_union([],B,B).
sets_union([H|T],B,R):- % do nothing in R, if H exists
    exists(B,H),
    !,
    sets_union(T,B,R).
sets_union([H|T],B,[H|R]):- % add it in R, if H doesn't exist
    % \+ exists(B,H),
    sets_union(T,B,R).

list_set([H],[H]).
list_set([H|T], E):-
    exists(T, H),
    !,
    list_set(T, E).
list_set([H|T],[H|R]):-
    list_set(T,R).
