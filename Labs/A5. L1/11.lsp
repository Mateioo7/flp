; 11
; a) Determine the least common multiple of the numerical values of a nonlinear list.

#| 
gcd-values(a, b) = {
	a, b == 0
	gcd-values(b, a%b), otherwise
} 
|#
(defun gcd-values(a b)
	(cond
		((= b 0) a)
		(t (gcd-values b (mod a b)))
	)
)

;lcm-values(a, b) = a*b / gcd-values(a, b)
(defun lcm-values(a b)
	(/ (* a b) (gcd-values a b))
)

;lcm-list(l) = lcm-list-solver(l2..ln, l1)
(defun lcm-list(l)
	(lcm-list-solver l 1)
)

#|
lcm-list-solver(l, res) {
	res, l is empty
	lcm-list-solver(l2..ln, lcm-values(res, l1)), l1 is number
	lcm-list-solver(l2..ln, lcm-list-solver(l1, res)), l1 is list
	lcm-list-solver(l2..ln, res), otherwise
}
|#
(defun lcm-list-solver(l res)
	(cond
		((null l) res)
		((numberp (car l)) 
			(lcm-list-solver (cdr l) (lcm-values res (car l))))
		((listp (car l)) 
			(lcm-list-solver (cdr l) (lcm-list-solver (car l) res)))
		(t (lcm-list-solver (cdr l) res))
	)
)
; (lcm-list '(12 16 A (12 C 16)))
; (lcm-list '(12 16 A (12 C 16) 96))
; (lcm-list '(12 96 A (12 C 16) 48))


; b) Write a function to test if a linear list of numbers has a "mountain" aspect. A list has a "mountain" aspect if the items increase to a certain point and then decrease. Eg. (10 18 29 17 11 10). The list must have at least 3 atoms to fullfil this criteria.

#|
mountain(l) = {
	false, l2 > l1
	mountain-check(1, l1, l2..ln), otherwise
}
|#
(defun mountain(l)
	(cond
		((> (car l) (car(cdr l))) nil)
		; to prevent (mountain '(23 20 ...)) case
		(t (mountain-check 1 (car l) (cdr l)))
	)
)

#|
0 - decreasing ; 1 - increasing
mountain-check(direction, previous, l) = {
	t, l is empty and direction == 0
	false, l is empty and direction == 1
	false, previous < l1 and direction == 0
	mountain-check(0, l1, l2..ln), previous > l1 and direction == 1
	mountain-check(direction, l1, l2..ln), otherwise
}
|#
(defun mountain-check(dir prev l)
	(cond
		((and (null l) (zerop dir)) t)
		((and (null l) (= dir 1)) nil)
		((and (< prev (car l)) (zerop dir)) nil)
		((and (> prev (car l)) (= dir 1))
			(mountain-check 0 (car l) (cdr l)))
		(t (mountain-check dir (car l) (cdr l)))
	)
)
; (mountain '(10 18 29 17 11 10))
; (mountain '(20 18 29 17 11 10))
; (mountain '(10 18 29 31 11 10))
; (mountain '(10 18 29 31 11 12))
; (mountain '(1 3 2))
; (mountain '(1 2 3))


; c) Remove all occurrences of a maximum numerical element from a nonlinear list.

;max-list(l) = max-list-solver(l2..ln, l1)
(defun max-list(l)
	(max-list-solver l -999999)
)
#|
max-list-solver(l, res) = {
	res, l is empty
	max-list-solver(l2..ln, max-list-solver(l1, res)), l1 is list
	max-list-solver(l2..ln, l1), l1 > res and l1 is number
	max-list-solver(l2..ln, res), otherwise
}
|#
(defun max-list-solver(l res)
	(cond
		((null l) res)
		((listp (car l))
			(max-list-solver (cdr l) (max-list-solver (car l) res)))
		((and (numberp (car l)) (> (car l) res))
			(max-list-solver (cdr l) (car l)))
		(t (max-list-solver (cdr l) res))
	)
)

#|
remove-max(l) = {
	remove-value(max-list(l), l)
}
|#
(defun remove-max(l)
	(remove-value(max-list l) l)
)

#|
remove-value(e, l) = {
	l, l is empty
	remove-value(e, l1) + remove-value(e, l2..ln), l1 is list
	l1 U remove-value(e, l2..ln), l1 is number and l1 != e
	l1 U remove-value(e, l2..ln), l1 is not number
	remove-value(e, l2..ln), otherwise
}
|#
(defun remove-value(e l)
	(cond
		((null l) nil)
		((listp (car l)) 
			(cons (remove-value e (car l)) (remove-value e (cdr l))))
		((and (numberp (car l)) (/= (car l) e))
		 	(cons (car l) (remove-value e (cdr l))))
		((not(numberp (car l)))
			(cons (car l) (remove-value e (cdr l))))
		(t (remove-value e (cdr l)))
	)
)
; (remove-max '(12 42 (42 (12 42) 13 B) 1 42 A (42)))


; d) Write a function which returns the product of numerical even atoms from a list, to any level.

; product-even(l) = product-even-solver(1, l)
(defun product-even(l)
	(product-even-solver 1 l)
)

#|
product-even-solver(res, l) = {
	res, l is empty
	product-even-solver(res*l1, l2..ln), l1 is even number
	product-even-solver(product-even-solver(res, l1), l2..ln), l1 is list
	product-even-solver(res, l2..ln), otherwise
}
|#
(defun product-even-solver(res l)
	(cond
		((null l) res) 
		((and (numberp (car l)) (zerop (mod (car l) 2)))
			(product-even-solver (* res (car l)) (cdr l)))
		((listp (car l))
			(product-even-solver (product-even-solver res (car l)) (cdr l)))
		(t (product-even-solver res (cdr l)))
	)
)
; (product-even '(1 2 (3 4 (5 6)) 7))













































;gcd-list(l) = gcd-list-solver(l2..ln, l1)
(defun gcd-list(l)
	(gcd-list-solver (cdr l) (car l))
)

#|
gcd-list-solver(l, res) = {
	res, l is empty
	gcd-list-solver(l2..ln, gcd-values(res, l1)), l1 is number
	gcd-list-solver(l2..ln, gcd-list-solver(l1, res)), l1 is list
	gcd-list-solver(l2..ln, res), otherwise
}
|#
(defun gcd-list-solver(l res)
	(cond
		((null l) res)
		((numberp (car l)) (gcd-list-solver (cdr l) (gcd-values res (car l))))
		((listp (car l)) (gcd-list-solver (cdr l) (gcd-list-solver (car l) res)))
		(t (gcd-list-solver (cdr l) res))
	)
)