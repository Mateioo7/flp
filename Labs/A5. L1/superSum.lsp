; superficial sum
(defun superSum(l)
	(cond
		((null l) 0)
		((numberp(car l)) (+ (car l) (sum(cdr l))))
		(t (sum(cdr l)))
	)
)