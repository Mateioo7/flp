; all elements sum
(defun sum(l)
	(cond
		((null l) 0)
		((numberp (car l)) (+ (car l) (sum (cdr l))))
		((atom (car l)) (sum (cdr l)))
		(t (+ (sum (car l)) (sum (cdr l))))
	)
)