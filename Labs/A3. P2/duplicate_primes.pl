% 10a. For a list of integer numbers, define a predicate to
% write twice in list every prime number.

% b. For a heterogeneous list, formed from integer numbers and
% list of numbers, define a predicate to write in every sublist
% twice every prime number.
%Eg.: [1, [2, 3], 4, 5, [1, 4, 6], 3, [1, 3, 7, 9, 10], 5] =>
%[1, [2, 2, 3, 3], 4, 5, [1, 4, 6], 3, [1, 3, 3, 7, 7, 9, 10], 5]

% prime(E,D), E - element to check, D - divisor
% (i,i) deterministic
% prime(E,D) =
% 1. false, if E<2
% 2. true, if D > E/D
% 3. false, if E%D = 0
% 4. prime(E,D+1)
prime(E,_):-
    E < 2,
    !,
    false.
prime(E,D):-
    D > div(E,D),
    !.
prime(E,D):-
    0 =:= mod(E,D),
    !,
    false.
prime(E,D):-
    D1 is D+1,
    prime(E,D1).

is_prime(E):-
    prime(E,2).

% 10.a duplicate_primes(L:input list, R:resulted list)
% (i,o),(o,i) deterministic
% duplicate_primes(L,R) =
% 1. [], if L is empty
% 2. l1 U l1 U duplicate_primes(l2l3..,R), if l1 is prime
% 3. l1 U duplicate_primes(l2l3..,R), otherwise
duplicate_primes([],[]).
duplicate_primes([H|T],[H,H|R]):-
    is_prime(H),
    !,
    duplicate_primes(T,R).
duplicate_primes([H|T],[H|R]):-
    duplicate_primes(T,R).


% (i) deterministic
% isList(E) = 1
% 1. true, if E is list
% 2. false, otherwise
isList([]).
isList([_|_]).


% 10.b duplicate_primes_sl(L:input list, R:resulted list)
% (i,o) deterministic
% d_p_s(L,R) =
% 1. [], if L is empty
% 2. duplicate_primes(l1,R1) U d_p_s(l2l3..,R), if l1 is sublist
% 3. l1 U d_p_s(l2l3..,R), otherwise
duplicate_primes_sl([],[]).
duplicate_primes_sl([H|T],[R1|R]):-
    isList(H),
    !,
    duplicate_primes(H,R1),
    duplicate_primes_sl(T,R).
duplicate_primes_sl([H|T],[H|R]):-
    duplicate_primes_sl(T,R).

% duplicate_primes_sl([1, [4, 3], 4, 11, [1, 4, 6], 3, [7], 5],R).



































% cut only after the condition (not the case if the condition
% is int the clause arguments; ex: exists([H|T,H):-... .)
