% 10. For a list a1... an with integer and distinct numbers,
% define a predicate to determine all subsets with sum of
% elements divisible with n.

% candidat(E:integer, L:list)
% (o, i) nedeterministic
% candidat(l1..ln) =
% 1. l1
% 2. candidat(l2..ln)
candidat(E, [H|_]):-
    E is H.
candidat(E, [_|T]):-
    candidat(E, T).


% sol(L:input list, N:integer sum, Res:result list)
% (i,i,o) nedeterministic
% solve(L,N) = solve_aux(L, N, [E], E), where E = candidat(L)
solve(L, N, Res):-
    candidat(E, L),
    solve_aux(L, N, Res, [E], E).


% sol_aux(L, N, Res, Sol:currentSolList, S:currentSumInteger)
% (i,i,o,i,i) nedeterministic
% sol_aux(L, N, Sol1..Soln, S) =
% 1. Sol1..Soln, if S%N = 0
% 2. sol_aux(L,N,E U Sol1..Soln,S+E) where E = candidat(L) and
% if E < Sol1
solve_aux(_, N, Res, Res, S):-
    S mod N =:= 0.
    %0 is mod(S, N).
solve_aux(L, N, Res, [H|T], S):-
    candidat(E, L),
    % line below is to prevent adding duplicate elements,
    % but more importantly to prevent having same solutions
    % "<" -> ascending results; ">" -> descending results
    E < H,
    NewSum is S+E,
    solve_aux(L, N, Res, [E|[H|T]], NewSum).
