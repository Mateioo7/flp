#| 
11. Return the level (and coresponded list of nodes) with maximum number of nodes for a binary tree of type (2). The level of the root element is 0. 
  A
 / \
B   C     -> (A 2 B 0 C 2 D 0 E 0) (1)		
   / \		 (A (B) (C (D) (E)))   (2)
  D   E
|#

;get-left-subtree(btree1..btreen) = btree2
(defun get-left-subtree(btree)
	(car(cdr btree)) ; <=> (cadr btree)
)

;get-right-subtree(btree1..btreen) = btree3
(defun get-right-subtree(btree)
	(car(cdr(cdr btree))) ; <=> (caddr btree)
)

#|
get-level(level, btree1..btreen) = {
	[], btree is empty
	[btree1], level = 0
	get-level(level-1, get-left-subtree(btree1..btreen)) 
	U
	get-level(level-1, get-right-subtree(btree1..btreen)), otherwise
}
|#
#|
cons = Theta(1)
append = Theta(n)
|#
(defun get-level (level btree)
	(cond
		((null btree) nil)
		((= level 0) (list (car btree))) ; we need list to avoid .
		(t (append (get-level (- level 1) (get-left-subtree btree))
				   (get-level (- level 1) (get-right-subtree btree))))
		; since we always append nil or a linear list, the result will be a linear list and we can define a linear get length function
	)
)

#|
get-len(l1..ln) = {
	0, l is empty
	1 + get-len(l2..ln), otherwise
}
|#
(defun get-len(l)
	(cond
		((null l) 0)
		(t (+ 1 (get-len (cdr l))))
	)
)

;get-max-level(btree) = _get-max-level(0, [], btree)
(defun get-max-level(btree)
	(_get-max-level 0 '() btree)
)

#|
_get-max-level(level, mx, btree) = {
	mx, currentLevel* is empty
	_get-max-level(level+1, currentLevel*, btree), get-len(currentLevel*) > get-len(max)
	_get-max-level(level+1, mx, btree), otherwise
	*currentLevel = get-level(level, btree)
}
|#
(defun _get-max-level(level mx btree)
	(setf currentLevel (get-level level btree))
	(cond
		((null currentLevel) mx)
		((> (get-len currentLevel) (get-len mx))
			(_get-max-level (+ level 1) currentLevel btree))
		(t (_get-max-level (+ level 1) mx btree))
	)
)
#|
(get-max-level '(A (B) (C (D) (E))))           				-> (B C)
  A
 / \
B   C	
   / \
  D   E

(get-max-level '(A (B (D) (E)) (C (F) (G))))   				-> (D E F G)
     A
   /   \
  B     C
 / \   / \
D   E F   G

(get-max-level '(A (B (D)) (C (F) (G))))       				-> (D F G)
     A
   /   \
  B     C
 / \   / \
D     F   G

(get-max-level '(A))                           				-> (A)
     A

(get-max-level '(A (B (D) (E (H))) (C (F (I)) (G)))) 		-> (D E F G)
      A
    /   \
  B       C
 / \     / \
D   E   F   G
   /   /
  H   I
|#